# Pronote API

This project aims to provide an easy-to-use interface to FliiFe/pronotefetcher with a JSON API.

## Installation

Grab the dependencies with the following:

```bash
git submodule update --init
npm install
```

You need the following environment variables:

```bash
export MYSQL_PASSWORD="your database password"
export PRONOTE_API_PORT=3000
export PRONOTE_API_SECRET="a private key you choose"
```

Import the database structure with

```bash
mysql -u root -p < database.sql
```

## API Reference

The API accepts only JSON input, and only spits JSON out.

### `/`, all methods

This is a no-op url. It just returns the api version

### `/login`, POST

You should give an object with the following structure :

```json
{
  "username": "<pronote username>",
  "password": "<pronote password>"
  "url": "<pronote desktop url>"
  "mobileUrl": "<pronote mobile url>"
}
```
To connect to the official Pronote demo, use:
```json
{
  "username": "demonstration",
  "password": "pronotevs"
  "url": "https://demo.index-education.net/pronote/eleve.html"
  "mobileUrl": "https://demo.index-education.net/pronote/mobile.eleve.html"
}
```
This should return something like that :

```json
{
  "token": "e3af950734974c128cd9bb8d479cb55524aa97c455f34b9ce324cc18216eb4ee",
  "error": false,
  "version": "X.X.X"
}
```

This token has to be given in the input to every request.

### `/fetch`, POST

Given a token, fetch data from pronote for the account corresponding to the token.

Input has to be of this form:

```json
{
  "token": "e3af950734974c128cd9bb8d479cb55524aa97c455f34b9ce324cc18216eb4ee"
}
```

And output will be of this form :

```json
{
  "error": false,
  "version": "X.X.X",
  "loginSuccess": true,
  "schedule": [
    {
      "classroom": "B_EPS",
      "date": "17/10/2016",
      "hour": "7h55",
      "isTeacherMissing": true,
      "notice": "Prof. Absent",
      "sub": "ED.PHYSIQUE &amp; SPORT.",
      "teacherName": "BIGO N."
    },
    ...
  ],
  "taf": [
    {
      "content": "Lecture cursive",
      "date": "17/10/2016",
      "sub": "FRANCAIS"
    },
    ...
  ],
  "marks": [
    {
      "subject": "FRANCAIS",
      "average": "Moyenne: 14,00",
      "marks": [
        {
          "coef": "1,00",
          "bareme": "20",
          "title": "Franceville dystopie",
          "value": "14,00"
        }, {
          "coef": "2,00",
          "bareme": "20",
          "title": "invention Utopie ",
          "value": "14,00"
        },
        ...
      ],
    },
    ...
  ],
}
```

### The `taf` array

The `taf` array contains the homeworks. `taf` stands for *Travail à faire*.

Each object in the array has this structure:

|    Key    |                                  Description                                  |     Example     |
|:---------:+:-----------------------------------------------------------------------------:+:---------------:|
|   `sub`   |                 The subject, in uppercase, without diacritics                 |     FRANCAIS    |
|   `date`  | The assignment has to be finished on that date.  Given in the form `dd/mm/yy` |    17/10/2016   |
| `content` |                             The assignment itself                             | Lecture Cursive |

### The `schedule` array

The `schedule` array contains schedule elements defined by this structure:

|         Key        |                                 Description                                |     Example    |
|:------------------:+:--------------------------------------------------------------------------:+:--------------:|
|     `classroom`    |                  The classroom the class takes place into                  |      C418      |
|       `date`       |               The class' date. Given in the form `dd/mm/yyyy`              |   17/10/2016   |
|       `hour`       |           Same as above, for the hour. Given in the form `12h34`           |      7h55      |
| `isTeacherMissing` |           Boolean. True if the teacher is missing for that class           |      false     |
|      `notice`      | Contains the header from Pronote. Defined only when pronote has defined it | "Cours annulé" |
|        `sub`       |                The subject, in uppercase, without diacritics               |  MATHEMATIQUES |
|    `teacherName`   |                             The teacher's name.                            |    DUPONT M.   |

### The `marks` array

The `marks` array contains the marks, along with per-subject averages and coefficients.

|    Key    |                                  Description                                  |     Example    |
|:---------:+:-----------------------------------------------------------------------------:+:--------------:|
| `subject` |                 The subject, in uppercase, without diacritics                 |    FRANCAIS    |
| `average` | Average for the subject, computed by Pronote, in the format: `Moyenne: 10,00` | Moyenne: 14,00 |
|  `marks`  |                  An array containing an object for each mark                  |       N/A      |

The `marks` property of each object contains objects with the following structure:

|    Key   |                   Description                   |                 Example                 |
|:--------:+:-----------------------------------------------:+:---------------------------------------:|
|  `coef`  |                   Coefficient                   |                   2,00                  |
| `bareme` | The maximum mark that could have been optained. | 20 (e.g. in the case of a 14 out of 20) |
|  `title` |          Name corresponding to the mark         |           Interrogation écrite          |
|  `value` |                 The mark itself                 |                  14,00                  |

### `loginSucess`

This key tells if login has been successful. If it has not, then you will get an error (`error: true`), and an error message. See below for more information

### Errors

All error outputs contain the key `error` with value `true`.

Every error come with an error message (property `errorMessage`).

If the input is not a proper JSON object or the object doesn't have a `token` property:
```json
{
  "error": true,
  "errorMessage": "Missing token",
  "version": "X.X.X"
}
```

If your token is invalid or expired, you will get this as result. Please notice tokens are always 64 characters long.
```json
{
  "error": true,
  "errorMessage": "Invalid or expired token",
  "version": "X.X.X"
}
```

This is an internal error, and should never show up.
```json
{
  "error": true,
  "errorMessage": "An error has occured while updating the database",
  "version": "X.X.X"
}
```

This one is an internal error too, and should never show up. It happends when login failed (wrong credentials, or changed credentials), but the API failed to remove the token entry from the database (*sucks, right ?*).
```json
{
  "error": true,
  "errorMessage": "Login failed, failed to remove token from database.",
  "version": "X.X.X"
}
```

And here is the last one. Login has failed (same reasons as above), and your token has been revoked. Get a new one with a call to `/login`.
```json
{
  "error": true,
  "errorMessage": "Login failed. Token has been removed. Please get a new one.",
  "version": "X.X.X"
}
```

## `/expire`, POST

This is used to force a token expiry. It takes a token as input.

```json
{
  "token": "e3af950734974c128cd9bb8d479cb55524aa97c455f34b9ce324cc18216eb4ee"
}
```

You should get an answer like that:
```json
{
  "error": false,
  "hasTokenExpired": true,
  "version": "X.X.X"
}
```

In case the token has already expired or is invalid, the api will return this:
```json
{
  "error": true,
  "errorMessage": "Token is invalid or has already expired",
  "version": "X.X.X"
}
```

## `/urls`, all methods

The API maintains a list of pronote urls along with school name.
This url gives you a list of urls.

```json
{
  "error": false,
  "version": "X.X.X",
  "urls": [
    {
      "name": "School Name (Town/City), site élève/parent",
      "url": "desktop pronote url",
      "mobileUrl": "mobile pronote url"
    }
  ],
}
```

## `/check-url`, POST

This endpoint can check if an address is a valid pronote url.
Curently, it is not fool-proof, and only tells if it is
*likely* that the url is a valid one

```javascript
{
  "error": false,
  "version": "X.X.X",
  "validUrl": true, //This value changes. True if the url is likely valid, false otherwise.
}
```

## `/add-url`, POST

This allows you to submit an url to the database.
Urls **have to be valid**, they will be rejected otherwise

The input is as follows:

```json
{
  "name": "School name (Town, Zip Code), site eleve/parent",
  "url": "desktop url",
}
```

> **Note**: You don't need to provide the mobileUrl anymore. Also, the school string is not checked upon any pattern rules or anything. Please use it, to keep a nice and neat database.

If the school has successfuly been added, then `error` will be false, and `urlAdded` true.

## `/get-urls`, POST

Given a pronote `url` (be whatever it is), and an `eleve` boolean value, return corresponding urls (mobile and desktop versions). If `eleve` is false, it is assumed you want the Parent version

Output will be of this form

```json
{
  "error": false,
  "version": "X.X.X",
  "url": "the desktop url",
  "mobileUrl": "the mobile url"
}
```

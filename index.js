var winston = require('winston');
var expressWinston = require('express-winston');
var express = require('express');
var app = express();
var fetcher = require('./fetcher.js');
var version = '1.0.0';
var sha256 = require('sha256');
var md5 = require('md5');
var mysql = require('mysql');

var connection = mysql.createConnection({
    host: 'localhost',
    user: 'root',
    password: process.env.MYSQL_PASSWORD || '',
    database: 'pronoteapi',
    multipleStatements: true,
});

connection.connect();

var bodyParser = require('body-parser');
app.use(bodyParser.json({
    type: '*/*'
}));

expressWinston.requestWhitelist = [];
expressWinston.requestWhitelist.push('body');
expressWinston.responseWhitelist.push('body');
app.use(expressWinston.logger({
    transports: [
        new winston.transports.Console({
            json: true,
            colorize: true
        })
    ],
    meta: true,
    expressFormat: true,
    colorize: true,
}));

function isTokenValid(token, callback) {
    connection.query('SELECT username,password,url,mobileUrl FROM tokens WHERE token=?', [token], function(err, result) {
        if (err) {
            console.trace(err);
            callback(false);
        } else {
            if (result && result.length === 1) {
                callback(true, result[0]);
            } else {
                callback(false);
            }
        }
    });
}

function generateToken(username, password, url, mobileUrl, isAccountValid, callback) {
    if (!isAccountValid) {
        callback(false);
        return;
    }
    var token = sha256.x2(process.env.PRONOTE_API_SECRET + Date.now());
    addTokenToDatabase(token, username, password, url, mobileUrl, Date.now(), callback);
    return token;
}

function updateLastUsed(token, callback) {
    connection.query('UPDATE tokens SET lastUsed=? WHERE token=?', [Date.now(), token], function(err) {
        if (err) {
            console.trace(err);
            callback(1);
        } else {
            callback(0);
        }
    });
}

function getPronoteUrls(pronoteUrl, eleve) {
    if (pronoteUrl.substr(8).indexOf('/') < 0) pronoteUrl += '/';
    while (pronoteUrl[pronoteUrl.length - 1] !== '/' || pronoteUrl.length === 0) {
        pronoteUrl = pronoteUrl.substr(0, pronoteUrl.length - 1);
    }
    var result = {
        url: pronoteUrl + (eleve ? 'eleve' : 'parent') + '.html',
        mobileUrl: pronoteUrl + 'mobile.' + (eleve ? 'eleve' : 'parent') + '.html'
    };
    return result;
}

function removeTokenFromDatabase(token, callback) {
    connection.query('DELETE FROM tokens WHERE token=?', [token], function(err) {
        if (err) {
            console.trace(err);
            callback(1);
        } else {
            callback(0);
        }
    });
}

function tokenExists(username, password, url, mobileUrl, callback) {
    console.log('Checking token presence for', username);
    var compatUrl = url + '?login=true&fd=1';
    var compatMobileUrl = mobileUrl + '?login=true&fd=1';
    connection.query('SELECT token FROM tokens WHERE username=? AND password=? AND url=? AND mobileUrl=?', [username, password, compatUrl, compatMobileUrl], function(err, results) {
        if (err) {
            console.trace(err);
            callback(undefined);
        } else {
            if (results.length >= 1) {
                callback(results[0].token);
            } else {
                callback(undefined);
            }
        }
    });
}

function addTokenToDatabase(token, username, password, url, mobileUrl, time, callback) {
    var compatUrl = url + '?login=true&fd=1';
    var compatMobileUrl = mobileUrl + '?login=true&fd=1';
    connection.query('INSERT INTO tokens VALUES (?,?,?,?,?,?,?)', [token, username, password, time, time, compatUrl, compatMobileUrl], function(err) {
        if (err) {
            console.trace(err);
            callback(1);
        } else {
            callback(0);
        }
    });
}

function getUrls(callback) {
    connection.query('SELECT * FROM pronote_urls', function(err, results) {
        if (err) {
            console.trace(err);
        } else {
            callback(results);
        }
    });
}

function addUrl(name, url, callback) {
    var correctUrl = getPronoteUrls(url, name.indexOf('site parent') === -1);
    connection.query('INSERT INTO pronote_urls (name,url,urlHash,mobileUrl,mobileUrlHash) VALUES (?,?,?,?,?)', [name, correctUrl.url, md5(correctUrl.url), correctUrl.mobileUrl, md5(correctUrl.mobileUrl)], function(err) {
        if (err) {
            console.trace(err);
            callback(false);
        } else {
            callback(true);
        }
    });
}

app.all('/urls', function(req, res) {
    getUrls(function(urls) {
        var array = [];
        urls.forEach(function(url) {
            array.push({
                name: url.name,
                url: url.url,
                mobileUrl: url.mobileUrl,
            });
        });
        res.send(JSON.stringify({
            error: false,
            version: version,
            urls: array,
        }));
    });
});

app.all('/', function(req, res) {
    res.send(JSON.stringify({
        error: false,
        version: version
    }));
});

app.post('/check-url', function(req, res) {
    if (!req.body.url) {
        res.send(JSON.stringify({
            error: true,
            errorMessage: 'No url provided',
            version: true
        }));
    } else {
        fetcher.validatePronoteUrl(req.body.url, function(urlSuccess) {
            res.send(JSON.stringify({
                error: false,
                version: version,
                validUrl: urlSuccess,
            }));
        });
    }
});

app.post('/get-urls', function(req, res) {
    if (!req.body.url) {
        res.send(JSON.stringify({
            error: true,
            errorMessage: 'No url provided',
            version: true
        }));
    } else {
        getPronoteUrls(req.body.url, req.body.eleve, function(urls) {
            res.send(JSON.stringify(Object.assign({
                error: false,
                version: version
            }, urls)));
        });
    }
});

app.post('/add-url', function(req, res) {
    console.log(req.body);
    var body = req.body;
    if (!body.name || !body.url) {
        res.send(JSON.stringify({
            error: true,
            errorMessage: 'Missing name or mobileUrl',
            version: version,
        }));
    } else {
        var urls = getPronoteUrls(body.url, body.name.indexOf('site parent') < 0);
        urls.url = urls.url + '?login=true&fd=1';
        urls.mobileUrl = urls.mobileUrl + '?login=true&fd=1';
        fetcher.validatePronoteUrl(urls.url, function(urlSuccess) {
            fetcher.validatePronoteUrl(urls.mobileUrl, function(mobileSuccess) {
                if (mobileSuccess && urlSuccess) {
                    addUrl(body.name, urls.url, function(dbSuccess) {
                        if (!dbSuccess) {
                            res.send(JSON.stringify({
                                error: true,
                                errorMessage: 'Url already exists',
                                urlAdded: false,
                                version: version,
                            }));
                            return;
                        }
                        res.send(JSON.stringify({
                            error: false,
                            version: version,
                            urlAdded: true,
                        }));
                    });
                } else {
                    res.send(JSON.stringify({
                        error: true,
                        errorMessage: 'Invalid url or mobileUrl',
                        urlAdded: false,
                        version: version,
                    }));
                }
            });
        });
    }
});

app.post('/fetch', function(req, res) {
    if (!req.body.token) {
        res.send(JSON.stringify({
            error: true,
            errorMessage: 'Missing token',
            version: version
        }));
        return;
    }
    isTokenValid(req.body.token, function(isValid, data) {
        if (!isValid) {
            res.send(JSON.stringify({
                error: true,
                errorMessage: 'Invalid or expired token',
                version: version,
            }));
        } else {
            fetcher.getData(data.username, data.password, data.url, function(fetchedData) {
                updateLastUsed(req.body.token, function(failure) {
                    if (failure) {
                        res.send(JSON.stringify({
                            error: true,
                            errorMessage: 'An error has occured while updating the database',
                            version: version,
                        }));
                    } else if (fetchedData.loginSuccess === false) {
                        removeTokenFromDatabase(req.body.token, function(failure) {
                            if (failure) {
                                res.send(JSON.stringify({
                                    error: true,
                                    errorMessage: 'Login failed, failed to remove token from database.',
                                    version: version,
                                }));
                            } else {
                                res.send(JSON.stringify({
                                    error: true,
                                    errorMessage: 'Login failed. Token has been removed. Please get a new one.',
                                    version: version,
                                }));
                            }
                        });
                    } else {
                        res.send(JSON.stringify(Object.assign({
                            error: false,
                            version: version,
                        }, fetchedData)));
                    }
                });
            });
        }
    });
});

app.post('/expire', function(req, res) {
    if (!req.body.token) {
        res.send(JSON.stringify({
            error: true,
            errorMessage: 'You should provide a token to make it expire',
            version: version
        }));
        return;
    }
    isTokenValid(req.body.token, function(isValid) {
        if (isValid) {
            removeTokenFromDatabase(req.body.token, function(failure) {
                if (!failure) {
                    res.send(JSON.stringify({
                        error: false,
                        hasTokenExpired: true, //Should always be true
                        version: version,
                    }));
                } else {
                    res.send(JSON.stringify({
                        error: true,
                        errorMessage: 'An error has occured while removing token from database',
                        version: version,
                    }));
                }
            });
        } else {
            res.send(JSON.stringify({
                error: true,
                errorMessage: 'Token is invalid or has already expired',
                version: version
            }));
        }
    });
});

app.post('/login', function(req, res) {
    console.log('Someone is trying to log in.');
    if (!req.body.username || !req.body.password || !req.body.url || !req.body.mobileUrl) {
        res.send(JSON.stringify({
            error: true,
            errorMessage: 'Missing username, password or URL (you should provide both url and mobileUrl)',
            version: version
        }));
        return;
    }
    tokenExists(req.body.username, req.body.password, req.body.url, req.body.mobileUrl, function(token) {
        if (token) {
            res.send(JSON.stringify({
                error: false,
                version: version,
                isAccountValid: true,
                token: token,
            }));
        } else {
            fetcher.validate(req.body.username, req.body.password, req.body.mobileUrl.replace(/(?:\?fd=1)?(.)$/g, '$1?fd=1'), function(isAccountValid) {
                token = generateToken(req.body.username, req.body.password, req.body.url, req.body.mobileUrl, isAccountValid, function(failure) {
                    if (failure) {
                        res.send(JSON.stringify({
                            error: true,
                            errorMessage: 'Failed to add token to database.',
                            version: version,
                        }));
                    } else {
                        res.send(JSON.stringify(Object.assign({
                            error: false,
                            version: version,
                            isAccountValid: isAccountValid,
                        }, isAccountValid ? {
                            token: token,
                        } : {})));
                    }
                });
            });
        }
    });
});

app.listen(parseInt(process.env.PRONOTE_API_PORT) || 3000, function() {
    console.log('API listening on port', process.env.PRONOTE_API_PORT || 3000);
});

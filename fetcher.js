'use strict';

var path = require('path');
var childProcess = require('child_process');
var phantomjs = require('phantomjs-prebuilt');
var shellescape = require('shell-escape');
var binPath = phantomjs.path;

module.exports.getData = function(username, password, url, callback) {
    var fetcherArgs = [path.join(__dirname, './pronotefetcher/app.js'), username, password];
    childProcess.execFile(binPath, fetcherArgs, {
        env: {
            'PRONOTE_URL': url,
        },
        timeout: 35000,
    }, function(err, stdout, stderr) {
        console.log(err, stderr);
        callback(JSON.parse(stdout));
    });
};
module.exports.validate = function(username, password, mobileUrl, callback) {
    var fetcherArgs = [path.join(__dirname, './pronotefetcher/checklogin.js'), username, password];
    childProcess.execFile(binPath, fetcherArgs, {
        env: {
            'PRONOTE_URL': mobileUrl,
        },
        timeout: 20000,
    }, function(err, stdout) {
        console.log(err, stdout);
        callback(stdout.indexOf('true') >= 0);
    });
};

module.exports.validatePronoteUrl = function(pronoteUrl, callback) {
    console.log('Validating url:', pronoteUrl);
    var shArgs = ['-c', 'curl ' + shellescape([pronoteUrl]) + ' 2>/dev/null | grep -oi \'<a href="http://www.index-education.com/"\' 1>/dev/null && echo true || echo false'];
    childProcess.execFile('sh', shArgs, {}, function(err, stdout) {
        callback(JSON.parse(stdout));
    });
};

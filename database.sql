SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";
CREATE DATABASE IF NOT EXISTS `pronoteapi` DEFAULT CHARACTER SET latin1 COLLATE latin1_swedish_ci;
USE `pronoteapi`;

DROP TABLE IF EXISTS `pronote_urls`;
CREATE TABLE `pronote_urls` (
  `id` int(11) NOT NULL,
  `name` text NOT NULL,
  `url` text NOT NULL,
  `urlHash` varchar(32) NOT NULL,
  `mobileUrl` text NOT NULL,
  `mobileUrlHash` varchar(32) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

DROP TABLE IF EXISTS `tokens`;
CREATE TABLE `tokens` (
  `token` varchar(64) NOT NULL,
  `username` text NOT NULL,
  `password` text NOT NULL,
  `timeOfCreation` int(11) NOT NULL,
  `lastUsed` int(11) NOT NULL,
  `url` text NOT NULL,
  `mobileUrl` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;


ALTER TABLE `pronote_urls`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `urlHash` (`urlHash`),
  ADD UNIQUE KEY `mobileUrlHash` (`mobileUrlHash`);

ALTER TABLE `tokens`
  ADD UNIQUE KEY `token` (`token`);


ALTER TABLE `pronote_urls`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
